package application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ResourceBundle;

import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import models.Salarie;
import models.Site;
import models.Service;

public class TableurController implements Initializable{
	
	@FXML
	private TableView<Salarie> salarieTable;
	
	@FXML
	private TableColumn<Salarie, String> nomCol;
	
	@FXML
	private TableColumn<Salarie, String> prenomCol;
	
	@FXML
	private TableColumn<Salarie, String> telFixeCol;
	
	@FXML
	private TableColumn<Salarie, String> telPortableCol;
	
	@FXML
	private TableColumn<Salarie, String> emailCol;
	
	@FXML
	private TableColumn<Salarie, String> serviceCol;
	
	@FXML
	private TableColumn<Salarie, String> siteCol;
	
	@FXML
	private ChoiceBox<String> ChoiceBoxService;
	
	@FXML
	private ChoiceBox<String> ChoiceBoxSite;
	
	@FXML
	private TextField searchBar;
	
	@FXML
	private TextField textInfoSalarie;
	
	@FXML
	private Parent fxml;
	
	@FXML
	private Pane infoPanneau;
	
	public TableurController() throws URISyntaxException, MalformedURLException, IOException {}
	
	public String getJson(String endPoint) throws MalformedURLException, IOException {
		
		String baseUrl = "http://localhost:8080/apiv1/webresources/";
		String url = baseUrl + endPoint + "/";
		
        URLConnection connection = new URL(url).openConnection();
        connection.setRequestProperty("User-Agent","thisisatest");
        connection.connect();
        
        InputStreamReader is = new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8);
        BufferedReader br = new BufferedReader(is);

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        
        return sb.toString();
	}
	
	private ObservableList<Salarie> getSalaries() throws MalformedURLException, IOException{
		
		//Récupère tableau de json sous forme de string
		String json = this.getJson("salaries");
		json = json.replace("\"salarie\":", "");
		json = json.replace("{[", "[");
		json = json.replace("]}", "]");
		        
		//Mapping du json en objet
		ObjectMapper mapper = new ObjectMapper();
		Salarie salariesArr[] = mapper.readValue(json, Salarie[].class);
		       
		ObservableList<Salarie> salariesList = FXCollections.observableArrayList();
		        
		for(int i = 0; i <= salariesArr.length-1; i++) {

			Salarie salarie = new Salarie();
			salarie.setId(salariesArr[i].getId());
			salarie.setNom(salariesArr[i].getNom());
			salarie.setPrenom(salariesArr[i].getPrenom());
			salarie.setTelFixe(salariesArr[i].getTelFixe());
			salarie.setTelPortable(salariesArr[i].getTelPortable());
			salarie.setEmail(salariesArr[i].getEmail());
			salarie.setService(salariesArr[i].getService());
			salarie.setSite(salariesArr[i].getSite());
			
			salariesList.add(salarie);
		}
		        
			System.out.println(salariesList);
				
			return salariesList;
	}
	
	private void filterByService(int serviceId) throws MalformedURLException, IOException{
		
		//Récupère tableau de json sous forme de string
		String json = this.getJson("salaries");
		json = json.replace("\"salarie\":", "");
		json = json.replace("{[", "[");
		json = json.replace("]}", "]");
		        
		//Mapping du json en objet
		ObjectMapper mapper = new ObjectMapper();
		Salarie salariesArr[] = mapper.readValue(json, Salarie[].class);
		       
		ObservableList<Salarie> salariesList = FXCollections.observableArrayList();
		        
		for(int i = 0; i <= salariesArr.length-1; i++) {

			Salarie salarie = new Salarie();
			salarie.setId(salariesArr[i].getId());
			salarie.setNom(salariesArr[i].getNom());
			salarie.setPrenom(salariesArr[i].getPrenom());
			salarie.setTelFixe(salariesArr[i].getTelFixe());
			salarie.setTelPortable(salariesArr[i].getTelPortable());
			salarie.setEmail(salariesArr[i].getEmail());
			salarie.setService(salariesArr[i].getService());
			salarie.setSite(salariesArr[i].getSite());
			
			if(salarie.getService() == serviceId || serviceId == 0) {
				
				salariesList.add(salarie);
			}
		}
		        
		System.out.println(salariesList);
				
		nomCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("nom"));
		prenomCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("prenom"));
		telFixeCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("telFixe"));
		telPortableCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("telPortable"));
		emailCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("email"));
		serviceCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("service"));
		siteCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("site"));
			
		salarieTable.setItems(salariesList);
	}
	
	private void filterBySite(int siteId) throws MalformedURLException, IOException{
		
		//Récupère tableau de json sous forme de string
		String json = this.getJson("salaries");
		json = json.replace("\"salarie\":", "");
		json = json.replace("{[", "[");
		json = json.replace("]}", "]");
		        
		System.out.println(json);
		
		//Mapping du json en objet
		ObjectMapper mapper = new ObjectMapper();
		Salarie salariesArr[] = mapper.readValue(json, Salarie[].class);
		       
		ObservableList<Salarie> salariesList = FXCollections.observableArrayList();
		        
		for(int i = 0; i <= salariesArr.length-1; i++) {

			Salarie salarie = new Salarie();
			salarie.setId(salariesArr[i].getId());
			salarie.setNom(salariesArr[i].getNom());
			salarie.setPrenom(salariesArr[i].getPrenom());
			salarie.setTelFixe(salariesArr[i].getTelFixe());
			salarie.setTelPortable(salariesArr[i].getTelPortable());
			salarie.setEmail(salariesArr[i].getEmail());
			salarie.setService(salariesArr[i].getService());
			salarie.setSite(salariesArr[i].getSite());
			
			if(salarie.getSite() == siteId || siteId == 0) {
				
				salariesList.add(salarie);
			}
		}
		        
		System.out.println(salariesList);
				
		nomCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("nom"));
		prenomCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("prenom"));
		telFixeCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("telFixe"));
		telPortableCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("telPortable"));
		emailCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("email"));
		serviceCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("service"));
		siteCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("site"));
		
		salarieTable.setItems(salariesList);
	}
	
	private ObservableList<Site> getSites() throws MalformedURLException, IOException {

		//Récupère tableau de json sous forme de string
		String json = this.getJson("sites");
		json = json.replace("\"site\":", "");
		json = json.replace("{[", "[");
		json = json.replace("]}", "]");
        
		//Mapping du json en objet
		ObjectMapper mapper = new ObjectMapper();
		Site sitesArr[] = mapper.readValue(json, Site[].class);
       
		ObservableList<Site> sitesList = FXCollections.observableArrayList();
        
		for(int i = 0; i <= sitesArr.length-1; i++) {

			Site site = new Site();
			site.setId(sitesArr[i].getId());
    	   	site.setVille(sitesArr[i].getVille());
        	
    	   	sitesList.add(site);
    	}
        
		System.out.println(json);
		
		return sitesList;

	}
	
	private ObservableList<Service> getServices() throws MalformedURLException, IOException {

		//Récupère tableau de json sous forme de string
		String json = this.getJson("services");
		json = json.replace("\"service\":", "");
		json = json.replace("{[", "[");
		json = json.replace("]}", "]");
        
		//Mapping du json en objet
		ObjectMapper mapper = new ObjectMapper();
		Service servicesArr[] = mapper.readValue(json, Service[].class);
       
		ObservableList<Service> servicesList = FXCollections.observableArrayList();
        
		for(int i = 0; i <= servicesArr.length-1; i++) {

			Service service = new Service();
			service.setId(servicesArr[i].getId());
			service.setNom(servicesArr[i].getNom());
        	
    	   	servicesList.add(service);
    	}
        
		System.out.println(json);
		
		return servicesList;

	}
	
	public void getChoiceSite(ActionEvent event) throws MalformedURLException, IOException {
		String site = ChoiceBoxSite.getValue();
		System.out.println(site);
		
		int siteId = 0;
		
		//Parcourir pour obtenir id du site à partir du nom
		ObservableList<Site> siteList = this.getSites();
		for(Site element : siteList) {
			if(element.getVille().equals(site)) {
				siteId = element.getId();
				System.out.println(siteId);
			}
			System.out.println(element);
		}
		
		this.filterBySite(siteId);
	}
	
	public void getChoiceService(ActionEvent event) throws MalformedURLException, IOException {
		String service = ChoiceBoxService.getValue();
		System.out.println(service);
		
		int serviceId = 0;
		
		//Parcourir pour obtenir id du service à partir du nom
		ObservableList<Service> servicesList = this.getServices();
		for (Service element : servicesList) {
			if(element.getNom().equals(service)) {
				serviceId = element.getId();
				System.out.println(serviceId);
			}
            System.out.println(element);
        }
		
		this.filterByService(serviceId);
	}
	
	@FXML
	void searchEmployee(ActionEvent event) throws MalformedURLException, IOException {
		ObservableList<Salarie> salariesList = this.getSalaries();
		ObservableList<Salarie> newSalariesList = FXCollections.observableArrayList();
		
		
		String search = searchBar.getText();
		
		for(Salarie element : salariesList) {
			if(element.getNom().equals(search) || element.getPrenom().equals(search)) {
				System.out.println("FIND");
				newSalariesList.add(element);
			}
		}
		
		salarieTable.getItems().clear();
		salarieTable.setItems(newSalariesList);
		
		if(search.equals("")) {
			salarieTable.getItems().clear();
			salarieTable.setItems(salariesList);
		}
		
		System.out.println(search);
		
	}
	
	@FXML
	public void selectEmployee(MouseEvent event) {
		
		Salarie selectedEmployee = salarieTable.getSelectionModel().getSelectedItem();
		
		System.out.println(selectedEmployee);
		
		textInfoSalarie.setText(selectedEmployee.toString());
		
//		infoPanneau.setText();
		
//		try {
//			fxml = FXMLLoader.load(getClass().getResource("/pageInfo.fxml"));
//			salarieTable.getChildren().removeAll();
//			ProduitsPage.getChildren().setAll(fxml);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		ObservableList<Site> sitesList = null;
		ObservableList<Salarie> salariesList = null;
		
		try {
			sitesList = this.getSites();
			salariesList = this.getSalaries();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		nomCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("nom"));
		prenomCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("prenom"));
		telFixeCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("telFixe"));
		telPortableCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("telPortable"));
		emailCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("email"));
		serviceCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("service"));
		siteCol.setCellValueFactory(new PropertyValueFactory<Salarie, String>("site"));
		
		salarieTable.setItems(salariesList);
		
		String[] sites = {"Tous", "Paris","Nantes","Toulouse", "Nice", "Lille"};
		String[] services = {"Tous", "Comptabilité","Production","Accueil", "Informatique", "Commercial"};
		
		
		
		ChoiceBoxService.getItems().addAll(services);
		ChoiceBoxSite.getItems().addAll(sites);
		
		ChoiceBoxSite.setOnAction(arg01 -> {
			try {
				getChoiceSite(arg01);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		ChoiceBoxService.setOnAction(arg01 -> {
			try {
				getChoiceService(arg01);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
	}
}
