module clientlourdbloc {
	requires javafx.controls;
	requires javafx.fxml;
	requires java.net.http;
	requires com.fasterxml.jackson.databind;
	requires javafx.base;
	requires com.fasterxml.jackson.annotation;
	requires com.fasterxml.jackson.core;
	requires javafx.graphics;
	
	exports models;
	
	opens application to javafx.graphics, javafx.fxml;
}
