package models;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonValue;

public class Site {
	
	private int id;
	private String ville;
	
	public Site() {
		
	}
	
	public Site(int id, String ville) {
		this.id = id;
		this.ville = ville;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	
	@Override
	@JsonValue
	public String toString() {
		return "Site [id=" + id + ", ville=" + ville + "]";
	}
	
}
